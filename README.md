# Field Logger

The project deals with creating a device to log all the Environmental parameters around it.

- The filed data logger could be used in many industries to monitor the conditions of any other device or a machine.
- The project could log upto 8 Analog input channels and upto 8 digital I/O pins

# Active Contributor
* [Anusha M@anushamaruprolu91](https://gitlab.com/anushamaruprolu91)

# Maintainer
* [rajat aggarwal@rajataggarwal2603](https://gitlab.com/rajataggarwal2603)
## ShunyaOS API

- The project will be built using shunyaOS interfaces library.
- The link for [Shunya documentation](http://demo.shunyaos.org/) 
- The link contains the steps to install the docker image and the documentation for the components to be used in the project.

## Architecture 

The architecture of the project is given below: <br>
[**Architecture**](https://drive.google.com/file/d/1qEiVDClrH0P6qz5DQc3_4gxBgPOw3PW4/view?usp=sharing)


## Overview
* The detailed project planning sheet for [**the project is here**](https://docs.google.com/spreadsheets/d/1AHfLIc6zGg4i4FTmj3yq_LLS0g7sW5kWmYsKiwy4ZoQ/edit?usp=sharing).
* All the updates status on the tasks could also be viewed on the planning sheet.

## Contributing to the project
- Anyone can contribute to the project by taking any task from an open issue.
- Fork the project.
- Select a task from the open issues and comment your deadline by tagging the maintainer @rajataggarwal2603
- Start working on the selected task from the issue .
- When a task is done submit a pull request to the master repository.
- The pull request should contain the code which is to be submiited in the **src** folder and the documentation for the same should be submitted to the **doc** folder
- When both code and documentation met the acceptance criteria, on then the issue will be deemed as closed.

## Gains for Contributor
- As the project is open source, anyone can contribute to the project.
- And the one who contributes the most in the respective project will be promoted to domain lead who will be mentioned in the project specifically.
- The contributor will get an insight of an industry relavant IoT project
- The contrbutor will get to work in a global community of contributors.
